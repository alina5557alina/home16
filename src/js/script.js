 alert('Theory in Readme file');
// 1
const string = "Hello";

function isPalindrome(str) {
  const reversedString = '';
  reverseString = str.split('').reverse().join('');
  if(str.toLowerCase() === reverseString.toLowerCase()) {
    console.log(true); 
  } else {
    console.log(false);
  }
}
isPalindrome(string);

//2

const line = prompt('Enter string');

function check(str){
  str.length > 20 ? console.log(false) : console.log(true); 
}
check(line);

//3


function howOld(data){
    const currentDate = new Date();
    
    const birthDateString = prompt('Enter your birthday in format YYYY-MM-DD');
    const birthDate = new Date(birthDateString);

    let age = currentDate.getFullYear() - birthDate.getFullYear();
    const monthDifference = currentDate.getMonth() - birthDate.getMonth();
    const dayDifference = currentDate.getDate() - birthDate.getDate();

    if (monthDifference < 0 || (monthDifference === 0 && dayDifference < 0)) {
        age--;
    }

    return age;
}

const age = howOld();
alert("Your age is " + age);
